﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Sandbox.Controllers;
using Sandbox.Services;
using Sitecore.DependencyInjection;

namespace Sandbox
{
    public class RegisterDependencies : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)

        {
            serviceCollection.AddTransient<IContactDataRepository, ContactDataRepository>();

            serviceCollection.AddTransient<ISendExmMessageService, SendExmMessageService>();
            serviceCollection.AddTransient(typeof(PageController));
            serviceCollection.AddTransient(typeof(CustomMessageController));
            serviceCollection.AddTransient(typeof(ProcessPersonalizationTokenController));
        }
    }
}