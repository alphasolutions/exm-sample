﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Sandbox.Services;
using Sitecore;
using Sitecore.Analytics;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Security.Authentication;
using Sitecore.Analytics.Model;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Data;
using Sitecore.Security.Accounts;

namespace Sandbox.Controllers
{
    public class PageController : Controller
    {
        private readonly ISendExmMessageService _exmMessageService;

        public PageController(ISendExmMessageService exmMessageService)
        {
            _exmMessageService = exmMessageService;
        }

        // GET: Page
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RenderPageContent()
        {

            return PartialView("~/Views/Sandbox/Renderings/PageContent.cshtml");
        }

        public ActionResult Login()
        {
            if (Context.IsLoggedIn)
            {
                Redirect("/Test Item");
            }
            if (AuthenticationManager.Login("extranet\\niket"))
            {
                if (Tracker.Enabled && Tracker.IsActive)
                {
                    var errors = new List<string>();
                    var user = Context.User;
                    var firstName = "Niket";//Context.User.Profile.FullName
                    var lastName = "Ashesh";
                    var company = "Sitecore";
                    var email = "niket.ashesh@gmail.com";
                    UpdateProfile(user, firstName, lastName, company, out errors);
                    var emailFacet = Tracker.Current.Contact.GetFacet<IContactEmailAddresses>(Constants.ContactFacetNames.Emails);
                    if (!emailFacet.Entries.Contains("Work Email"))
                    {
                        var emailaddress = emailFacet.Entries.Create("Work Email");
                        emailaddress.SmtpAddress = email;
                        emailFacet.Preferred = "Work Email";
                    }
                    var identifiers = Tracker.Current.Contact.Identifiers;
                    //since the user is not logged in we'll set the identifier
                    if (identifiers.IdentificationLevel != ContactIdentificationLevel.Known)
                    {
                        Tracker.Current.Session.Identify(email);
                    }
                }
                Redirect("/Test Item");
            }

            return Redirect("/");
        }

        public ActionResult SendEmail()
        {
            _exmMessageService.Send(ID.Parse("{508277E8-745D-4859-B2E7-268CC7E90A27}"), Context.User.Profile.Email, new Dictionary<string, object>(0));
            return new EmptyResult();
        }

        public ActionResult SendEmailToUser(string email)
        {
            var customPersonsToken = new Dictionary<string, object>
            {
                {"DynamicToken", $"Custom Token was replaced for {email}. This is a Web Forms email"}
            };
            _exmMessageService.Send(ID.Parse("{4E9F9E07-ACBD-4042-B173-B9C64F001048}"), email, customPersonsToken);
            return new EmptyResult();
        }
        public ActionResult SendMVCEmailToUser(string email)
        {
            var customPersonsToken = new Dictionary<string, object>
            {
                {"CustomTokenExample", $"Custom Token was replaced for {email}. This is a MVC email"}
            };
            _exmMessageService.Send(ID.Parse("{246FCA85-95C0-40C8-A766-5CDD8EC974BC}"), email, customPersonsToken);
            return new EmptyResult();
        }
        private bool UpdateProfile(User user, string firstName, string lastName, string jobTitle, out List<string> errors)
        {
            errors = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                {
                    errors.Add(Translate.Text("Error Profile First and Last Name Required"));
                    return false;
                }
                if (!user.IsAuthenticated)
                {
                    errors.Add(Translate.Text("Error Profile Not Logged In"));
                    return false;
                }
                //update sitecore user profile
                var userProfile = user.Profile;
                userProfile.FullName = $"{firstName} {lastName}";
                userProfile.Save();

                //update contact card information
                if (Tracker.IsActive && Tracker.Current.Contact != null)
                {
                    var contactInfo = Tracker.Current.Contact.GetFacet<IContactPersonalInfo>(Constants.ContactFacetNames.Personal);
                    contactInfo.FirstName = firstName;
                    contactInfo.Surname = lastName;
                    contactInfo.JobTitle = jobTitle;
                }
            }
            catch (Exception ex)
            {
                Log.Error("could not update profile", ex, this);
                errors.Add(Translate.Text("Error Updating Profile"));
                return false;
            }
            return true;
        }

        #region helper code

        public void Additional()
        {
            //AutomationStateManager automationStateManager = Tracker.Current.Session.CreateAutomationStateManager();
            //automationStateManager.EnrollInEngagementPlan();
        }
        #endregion
    }
}