﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sandbox.Extensions.ContentSearch.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;

namespace Sandbox.Controllers
{
    public class SearchExampleController : Controller
    {

        [HttpGet]
        public ActionResult RenderResults()
        {

            using (var searchContext =
                (ContentSearchManager.GetIndex("sitecore_master_index").CreateSearchContext()))
            {
                //var original = searchContext.GetQueryable<SearchResultItem>().Where(p => p.Name.Contains("a"))
                //    .GetResults();
                var result = searchContext.GetQueryable<SearchResultItem>().Where(p => p.Content.Contains("a"));
                var realResults = result.GetResultsWithHighlights(new List<string>() { "_content" });

                return Json(realResults, JsonRequestBehavior.AllowGet);
            }
        }
    }
}