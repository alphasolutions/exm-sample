﻿using System.Collections.Generic;
using System.Web.Mvc;
using Sandbox.Models;
using Sandbox.Services;
using Sitecore.Data;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core.Gateways;
using Sitecore.Modules.EmailCampaign.Factories;
using Sitecore.Mvc.Controllers;
using Sitecore.Web;

namespace Sandbox.Controllers
{
    public class CustomMessageController : SitecoreController
    {
        private readonly AnalyticsGateway _gateway;
        private readonly IContactDataRepository _contactDataRepository;

        public CustomMessageController(IContactDataRepository contactDataRepository)
        {
            _contactDataRepository = contactDataRepository;
            _gateway = EcmFactory.GetDefaultFactory().Gateways.AnalyticsGateway;
        }
        // GET: Page
        public override ActionResult Index()
        {
            var contactIdQueryKey = GlobalSettings.AnalyticsContactIdQueryKey;
            var contactIdString = WebUtil.GetQueryString(contactIdQueryKey);
            ShortID contactId;
            if (string.IsNullOrEmpty(WebUtil.GetQueryString(GlobalSettings.EcmIdQueryStringKey))||string.IsNullOrEmpty(contactIdString) || !ShortID.TryParse(contactIdString, out contactId))
                return Content("A list of prducts will render here");
            var contact = _gateway.GetReadonlyContact(contactId.Guid);
            if (contact == null)
                return new EmptyResult();
            var model = new List<ProductModel>()
            {
                new ProductModel() {Name = _contactDataRepository.GetEmailForContact(contact)},
                new ProductModel() {Name = _contactDataRepository.GetExternalSystemIdentifier(contact) }
            };

            return PartialView("~/Views/Sandbox/Renderings/ProductList.cshtml", model);

        }


    }
}