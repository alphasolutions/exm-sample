﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.ExM.Framework.Diagnostics;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core.Gateways;
using Sitecore.Modules.EmailCampaign.Core.Personalization;
using Sitecore.Modules.EmailCampaign.Factories;
using Sitecore.Modules.EmailCampaign.Recipients;
using Sitecore.Modules.EmailCampaign.Xdb;
using Sitecore.Mvc.Controllers;
using Sitecore.Text;
using Sitecore.Web;

namespace Sandbox.Controllers
{
    public class ProcessPersonalizationTokenController : SitecoreController
    {
        private readonly ILogger _logger;
        private readonly AnalyticsGateway _gateway;

        public ProcessPersonalizationTokenController()
        {
            _logger = Logger.Instance;
            _gateway = EcmFactory.GetDefaultFactory().Gateways.AnalyticsGateway;

        }
        // GET: Page
        public override ActionResult Index()
        {
            //todo:need to understand this
            if (!string.IsNullOrEmpty(WebUtil.GetQueryString(GlobalSettings.EcmIdQueryStringKey)))
                return new EmptyResult();
            var contactIdQueryKey = GlobalSettings.AnalyticsContactIdQueryKey;
            var contactIdString = WebUtil.GetQueryString(contactIdQueryKey);
            ShortID contactId;
            if (string.IsNullOrEmpty(contactIdString) || !ShortID.TryParse(contactIdString, out contactId))
                return new EmptyResult();
            var contact = _gateway.GetReadonlyContact(contactId.Guid);
            if (contact == null)
                return
                    new EmptyResult();
            var urlString = new UrlString(WebUtil.GetRequestUri().AbsoluteUri);
            urlString.Truncate(contactIdQueryKey);
            var input = Sitecore.Modules.EmailCampaign.Core.HtmlHelper.DownloadStringContent(urlString.ToString());
            if (string.IsNullOrEmpty(input))
            {
                _logger.LogWarn($"Cannot download the '{urlString}' page.");
            }
            else
            {
                var source = new HashSet<Token>();
                foreach (Capture match in Regex.Matches(input, "\\$\\w+\\$", RegexOptions.IgnoreCase))
                {
                    var token = new Token(match.Value.Trim('$'));
                    source.Add(token);
                }
                if (source.Count == 0)
                    return new EmptyResult();
                var profileValues = GetProfileValues(contactId.Guid, source);
                if (profileValues == null)
                    return new EmptyResult();
                string values = $"['{(object)string.Join("','", profileValues.ToArray())}']";
                var script = AddScript(
                     $"['{(object)string.Join("','", source.Select(t => t.Key).ToArray())}']", values);
                return Content(script);
            }
            return new EmptyResult();
        }

        protected string AddScript(string tokens, string values)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("<script type='text/javascript'>\n    function replaceTokens(){\n        document.forms[0].innerHTML = replaceTokensInString(document.forms[0].innerHTML);\n        document.title = replaceTokensInString(document.title);\n    }\n\n    function replaceTokensInString(str) {\n        var names = ");
            stringBuilder.Append(tokens);
            stringBuilder.Append(";\n        var values =");
            stringBuilder.Append(values);
            stringBuilder.Append(";\n\n        for (var i = 0; i < names.length; i++)\n        {\n            var re = new RegExp('\\\\$' + names[i] + '\\\\$', 'g');\n            str = str.replace(re, values[i]);\n        }\n        return str; \n    }\n\n    window.onload = replaceTokens;\n</script>");
            return
             stringBuilder.ToString();
        }

        protected List<string> GetProfileValues(Guid contactId, IEnumerable<Token> tokens)
        {
            var recipient = RecipientRepository.GetDefaultInstance().GetRecipient(new XdbContactId(contactId));
            if (recipient == null)
                return null;
            var personalizationManager = new PersonalizationManager(new RecipientPropertyTokenMapper(recipient));
            return tokens.Select(personalizationManager.ResolveTokenValue).Where(tokenValue => tokenValue.KnownToken).Select(value => value.Value?.ToString() ?? string.Empty).ToList();
        }
    }
}
