How to get started

1. Install Sitecore.NET 8.2 (rev. 161221)

2. Add a new nuget package source, in Visual studio https://sitecore.myget.org/F/sc-packages/api/v3/index.json 

3. ![1074102279-nuget src.png](https://bitbucket.org/repo/ekr486j/images/3278753889-1074102279-nuget%20src.png)

4. Follow the set up instructions here http://blog.alpha-solutions.us/2017/04/how-to-send-an-exm-email-from-code/

5. Modify your local code to and publish

6. Write a controller rendering and use the ISendExmMessageService's Send method to send a new message