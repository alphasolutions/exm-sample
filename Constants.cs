﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sandbox
{
    public class Constants
    {
        public struct ContactFacetNames
        {
            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactPersonalInfo
            /// </summary>
            public const string Personal = "Personal";

            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactAddresses
            /// </summary>
            public const string Addresses = "Addresses";

            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactEmailAddresses
            /// </summary>
            public const string Emails = "Emails";

            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactPhoneNumbers
            /// </summary>
            public const string PhoneNumbers = "Phone Numbers";

            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactPicture
            /// </summary>
            public const string Picture = "Picture";

            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactCommunicationProfile
            /// </summary>
            public const string CommunicationProfile = "Communication Profile";

            /// <summary>
            /// mapped to Sitecore.Analytics.Model.Entities.IContactPreferences
            /// </summary>
            public const string Preferences = "Preferences";

            /// <summary>
            /// mapped to Sitecore.Social.Connector.Facets.Contact.SocialProfile.ISocialProfileFacet
            /// </summary>
            public const string SocialProfile = "SocialProfile";

            public const string ContactExternalSystemIdentifiers = "External System Identifiers";


        }
    }
}