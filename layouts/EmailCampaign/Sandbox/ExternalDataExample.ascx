﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExternalDataExample.ascx.cs" Inherits="Sandbox.layouts.EmailCampaign.Sandbox.ExternalDataExample" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:Panel runat="server" ID="pnlPlaceholderText">
    <h3>This will be replaced with product list</h3>
</asp:Panel>
<asp:Panel runat="server" ID="pnlListView" Visible="False">
    <asp:ListView runat="server" ID="lvProductList" OnItemDataBound="lvProductList_OnItemDataBound">
        <LayoutTemplate>
            <ul>
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder"></asp:PlaceHolder>
            </ul>
        </LayoutTemplate>
        <ItemTemplate>
            <li>
                <asp:Literal runat="server" ID="litText"></asp:Literal>
            </li>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
