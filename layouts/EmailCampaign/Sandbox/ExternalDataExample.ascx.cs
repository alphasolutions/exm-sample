﻿using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.ExM.Framework.Diagnostics;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Modules.EmailCampaign.Core.Personalization;
using Sitecore.Modules.EmailCampaign.Factories;
using Sitecore.Modules.EmailCampaign.Recipients;
using Sitecore.Modules.EmailCampaign.Xdb;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sandbox.Extensions.Entities;
using Sandbox.Services;
using Sitecore.Analytics.Data;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Tracking;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core.Gateways;
using Literal = System.Web.UI.WebControls.Literal;


namespace Sandbox.layouts.EmailCampaign.Sandbox
{

    public partial class ExternalDataExample : System.Web.UI.UserControl
    {
        private readonly ILogger logger;
        private readonly AnalyticsGateway _gateway;
        private readonly IContactDataRepository _contactDataRepository;

        public ExternalDataExample(IContactDataRepository contactDataRepository)
        {
            _contactDataRepository = contactDataRepository;
            logger = Logger.Instance;
            _gateway = EcmFactory.GetDefaultFactory().Gateways.AnalyticsGateway;

        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (!Sitecore.Context.PageMode.IsNormal)
                return;
            try
            {
                var contactIdQueryKey = GlobalSettings.AnalyticsContactIdQueryKey;
                var contactIdString = WebUtil.GetQueryString(contactIdQueryKey);
                ShortID contactId;
                if (string.IsNullOrEmpty(contactIdString) || !ShortID.TryParse(contactIdString, out contactId))
                    return;
                var contact = _gateway.GetReadonlyContact(contactId.Guid);
                if (contact == null) return;
                lvProductList.DataSource = new[] { _contactDataRepository.GetEmailForContact(contact), _contactDataRepository.GetExternalSystemIdentifier(contact), "Product 2" };
                lvProductList.DataBind();
                pnlPlaceholderText.Visible = false;
                pnlListView.Visible = true;
            }
            catch (Exception ex)
            {
                pnlPlaceholderText.Visible = pnlListView.Visible = false;
                logger.LogError("could not render product list", ex);
            }

        }



        protected void lvProductList_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                // Display the e-mail address in italics.
                var litText = (Literal)e.Item.FindControl("litText");
                litText.Text = e.Item.DataItem.ToString();
            }
        }
    }
}