﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sandbox.Extensions.EmailCampaign;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core.Personalization;
using Sitecore.Modules.EmailCampaign.Core.Pipelines.DispatchNewsletter;
using Sitecore.Modules.EmailCampaign.Messages;

namespace Sandbox.Extensions.Pipelines
{
    public class ModifyMessageBody
    {
        public void Process(DispatchNewsletterArgs args)
        {
            Assert.ArgumentNotNull((object)args, "args");
            var mail = args.Message;
            if (mail != null && mail is WebPageMail && mail.CustomPersonTokens.ContainsKey("DynamicToken"))
            {
                var tokenValue = mail.CustomPersonTokens["DynamicToken"];
                Console.WriteLine(mail.Body);
            }
        }

    }

}