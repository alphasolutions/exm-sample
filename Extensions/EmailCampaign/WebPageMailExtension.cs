﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Modules.EmailCampaign.Core.Personalization;
using Sitecore.Modules.EmailCampaign.Messages;

namespace Sandbox.Extensions.EmailCampaign
{
    public class WebPageMailExtension : WebPageMail
    {
        public WebPageMailExtension(Item item) : base(item)
        {

        }
        public override object Clone()
        {
            var newMessage = new WebPageMailExtension(this.InnerItem);
            CloneFields(newMessage);
            return newMessage;
        }
        public virtual string ReplaceDynamicTokens(string messageBody)
        {
            var utcNow = DateTime.UtcNow;
            var token = new Token("CustomTokenExample");
            object newValue;
            var tokenValue = PersonalizationManager?.ResolveTokenValue(token);
            if (tokenValue?.Value != null)
                newValue = tokenValue.Value;
            Util.TraceTimeDiff("  ReplaceTokens -> Find/Add $title$ token", utcNow);
            return string.Empty;
        }

        public WebPageMailExtension(Item item, MessageBodyCache messageBodyCache) : base(item, messageBodyCache)
        {
        }
    }
}