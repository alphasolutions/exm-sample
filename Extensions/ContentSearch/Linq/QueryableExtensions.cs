﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Sitecore.Abstractions;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Pipelines.QueryGlobalFilters;
using Sitecore.ContentSearch.SolrProvider;
using Sitecore.Diagnostics;

namespace Sandbox.Extensions.ContentSearch.Linq
{
    public static class QueryableExtensions
    {
        public static SearchResultsWithHighlights<TSource> GetResultsWithHighlights<TSource>(this IQueryable<TSource> source, List<string> fieldNames)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            return source.Provider.Execute<SearchResultsWithHighlights<TSource>>(Expression.Call(null,
                ((MethodInfo)MethodBase.GetCurrentMethod()).MakeGenericMethod(typeof(TSource)), new Expression[2]
            {
                source.Expression,
                Expression.Constant(fieldNames)
            }));
           
        }
    }
}