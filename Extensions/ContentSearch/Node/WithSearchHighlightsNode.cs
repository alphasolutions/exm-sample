﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.ContentSearch.Linq.Nodes;

namespace Sandbox.Extensions.ContentSearch.Node
{
    public class WithSearchHighlightsNode : QueryNode
    {
        public WithSearchHighlightsNode(QueryNode queryNode, List<string> highlightFieldNames)
        {
            SourceNode = queryNode;
            HighlightFieldNames = highlightFieldNames;
        }
        public QueryNode SourceNode { get; protected set; }

        public List<string> HighlightFieldNames { get; protected set; }
        public override QueryNodeType NodeType => QueryNodeType.Custom;
        public override IEnumerable<QueryNode> SubNodes { get { yield return SourceNode; } }
    }
}