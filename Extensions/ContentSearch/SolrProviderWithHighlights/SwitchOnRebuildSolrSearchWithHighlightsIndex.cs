﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Maintenance;

namespace Sandbox.Extensions.ContentSearch.SolrProviderWithHighlights
{
    public class SwitchOnRebuildSolrSearchWithHighlightsIndex : Sitecore.ContentSearch.SolrProvider.SwitchOnRebuildSolrSearchIndex
    {
        public SwitchOnRebuildSolrSearchWithHighlightsIndex(string name, string core, string rebuildcore, IIndexPropertyStore propertyStore) : base(name, core, rebuildcore, propertyStore)
        {
        }

        public override IProviderSearchContext CreateSearchContext(Sitecore.ContentSearch.Security.SearchSecurityOptions options = Sitecore.ContentSearch.Security.SearchSecurityOptions.EnableSecurityCheck)
        {
            return new SolrSearchWithHighlightsContext(this, options);
        }
    }
}
