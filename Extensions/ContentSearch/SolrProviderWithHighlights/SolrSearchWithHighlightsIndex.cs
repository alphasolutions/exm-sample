﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Maintenance;

namespace Sandbox.Extensions.ContentSearch.SolrProviderWithHighlights
{
    public class SolrSearchWithHighlightsIndex : Sitecore.ContentSearch.SolrProvider.SolrSearchIndex
    {
        public SolrSearchWithHighlightsIndex(string name, string core, IIndexPropertyStore propertyStore, string @group) : base(name, core, propertyStore, @group)
        {
        }

        public SolrSearchWithHighlightsIndex(string name, string core, IIndexPropertyStore propertyStore) : base(name, core, propertyStore)
        {
        }

        public override IProviderSearchContext CreateSearchContext(Sitecore.ContentSearch.Security.SearchSecurityOptions options = Sitecore.ContentSearch.Security.SearchSecurityOptions.EnableSecurityCheck)
        {
            return new SolrSearchWithHighlightsContext(this, options);
        }
    }
}
