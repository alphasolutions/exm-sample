﻿using System.IO;
using Sitecore.ContentSearch.Linq.Common;

namespace Sandbox.Extensions.ContentSearch.SolrProviderWithHighlights
{
    public class GenericDumpable : IDumpable
    {
        protected object Value { get; set; }

        public GenericDumpable(object value)
        {
            Value = value;
        }

        public virtual void WriteTo(TextWriter writer)
        {
            var dumpable = this.Value as IDumpable;
            if (dumpable != null)
                dumpable.WriteTo(writer);
            else
                writer.WriteLine(this.Value);
        }
    }
}