﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Abstractions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Pipelines.IndexingFilters;
using Sitecore.ContentSearch.Security;
using Sitecore.ContentSearch.SolrProvider;
using SolrNet;

namespace Sandbox.Extensions.ContentSearch.SolrProviderWithHighlights
{
    public class SolrSearchResultsEx<TElement>
    {
        private readonly SolrSearchContext context;
        private readonly SolrQueryResults<Dictionary<string, object>> searchResults;
        private readonly SolrIndexConfiguration solrIndexConfiguration;
        private readonly IIndexDocumentPropertyMapper<Dictionary<string, object>> mapper;
        private readonly SelectMethod selectMethod;
        private readonly IEnumerable<IExecutionContext> executionContexts;
        private readonly IEnumerable<IFieldQueryTranslator> virtualFieldProcessors;
        private readonly int numberFound;

        public int NumberFound
        {
            get
            {
                return this.numberFound;
            }
        }

        public SolrSearchResultsEx(SolrSearchContext context, SolrQueryResults<Dictionary<string, object>> searchResults, SelectMethod selectMethod, IEnumerable<IExecutionContext> executionContexts, IEnumerable<IFieldQueryTranslator> virtualFieldProcessors)
        {
            this.context = context;
            this.solrIndexConfiguration = (SolrIndexConfiguration)this.context.Index.Configuration;
            this.selectMethod = selectMethod;
            this.virtualFieldProcessors = virtualFieldProcessors;
            this.executionContexts = executionContexts;
            this.numberFound = searchResults.NumFound;
            this.searchResults = SolrSearchResultsEx<TElement>.ApplySecurity(searchResults, context.SecurityOptions, context.Index.Locator.GetInstance<ICorePipeline>(), context.Index.Locator.GetInstance<IAccessRight>(), ref this.numberFound);
            OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>> executionContext = this.executionContexts != null ? this.executionContexts.FirstOrDefault<IExecutionContext>((Func<IExecutionContext, bool>)(c => c is OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>>)) as OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>> : (OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>>)null;
            this.mapper = (executionContext != null ? executionContext.OverrideObject : (IIndexDocumentPropertyMapper<Dictionary<string, object>>)null) ?? this.solrIndexConfiguration.IndexDocumentPropertyMapper;
        }

        private static SolrQueryResults<Dictionary<string, object>> ApplySecurity(SolrQueryResults<Dictionary<string, object>> solrQueryResults, SearchSecurityOptions options, ICorePipeline pipeline, IAccessRight accessRight, ref int numberFound)
        {
            if (!options.HasFlag((Enum)SearchSecurityOptions.DisableSecurityCheck))
            {
                HashSet<Dictionary<string, object>> dictionarySet = new HashSet<Dictionary<string, object>>();
                foreach (Dictionary<string, object> dictionary in solrQueryResults.Where<Dictionary<string, object>>((Func<Dictionary<string, object>, bool>)(searchResult => searchResult != null)))
                {
                    object obj1;
                    if (dictionary.TryGetValue("_uniqueid", out obj1))
                    {
                        object obj2;
                        dictionary.TryGetValue("_datasource", out obj2);
                        if (OutboundIndexFilterPipeline.CheckItemSecurity(pipeline, accessRight, new OutboundIndexFilterArgs((string)obj1, (string)obj2)))
                        {
                            dictionarySet.Add(dictionary);
                            numberFound = numberFound - 1;
                        }
                    }
                }
                foreach (Dictionary<string, object> dictionary in dictionarySet)
                    solrQueryResults.Remove(dictionary);
            }
            return solrQueryResults;
        }

        public TElement ElementAt(int index)
        {
            if (index < 0 || index > this.searchResults.Count)
                throw new IndexOutOfRangeException();
            return this.mapper.MapToType<TElement>(this.searchResults[index], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public TElement ElementAtOrDefault(int index)
        {
            if (index < 0 || index > this.searchResults.Count)
                return default(TElement);
            return this.mapper.MapToType<TElement>(this.searchResults[index], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public bool Any()
        {
            return this.numberFound > 0;
        }

        public long Count()
        {
            return (long)this.numberFound;
        }

        public TElement First()
        {
            if (this.searchResults.Count < 1)
                throw new InvalidOperationException("Sequence contains no elements");
            return this.ElementAt(0);
        }

        public TElement FirstOrDefault()
        {
            if (this.searchResults.Count < 1)
                return default(TElement);
            return this.ElementAt(0);
        }

        public TElement Last()
        {
            if (this.searchResults.Count < 1)
                throw new InvalidOperationException("Sequence contains no elements");
            return this.ElementAt(this.searchResults.Count - 1);
        }

        public TElement LastOrDefault()
        {
            if (this.searchResults.Count < 1)
                return default(TElement);
            return this.ElementAt(this.searchResults.Count - 1);
        }

        public TElement Single()
        {
            if (this.Count() < 1L)
                throw new InvalidOperationException("Sequence contains no elements");
            if (this.Count() > 1L)
                throw new InvalidOperationException("Sequence contains more than one element");
            return this.mapper.MapToType<TElement>(this.searchResults[0], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public TElement SingleOrDefault()
        {
            if (this.Count() == 0L)
                return default(TElement);
            if (this.Count() == 1L)
                return this.mapper.MapToType<TElement>(this.searchResults[0], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
            throw new InvalidOperationException("Sequence contains more than one element");
        }

        public IEnumerable<SearchHit<TElement>> GetSearchHits()
        {
            foreach (Dictionary<string, object> searchResult in (List<Dictionary<string, object>>)this.searchResults)
            {
                float score = -1f;
                object obj;
                if (searchResult.TryGetValue("score", out obj) && obj is float)
                    score = (float)obj;
                yield return new SearchHit<TElement>(score, this.mapper.MapToType<TElement>(searchResult, this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions));
            }
            List<Dictionary<string, object>>.Enumerator enumerator = new List<Dictionary<string, object>>.Enumerator();
        }

        public IEnumerable<TElement> GetSearchResults()
        {
            foreach (Dictionary<string, object> searchResult in (List<Dictionary<string, object>>)this.searchResults)
                yield return this.mapper.MapToType<TElement>(searchResult, this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
            var enumerator = new List<Dictionary<string, object>>.Enumerator();
        }

        public Dictionary<string, ICollection<KeyValuePair<string, int>>> GetFacets()
        {
            IDictionary<string, ICollection<KeyValuePair<string, int>>> facetFields = searchResults.FacetFields;
            IDictionary<string, IList<Pivot>> pivotFacets = searchResults.FacetPivots;

            var finalresults = facetFields.ToDictionary(x => x.Key, x => x.Value);

            if (pivotFacets.Count > 0)
            {
                foreach (var pivotFacet in pivotFacets)
                {
                    finalresults[pivotFacet.Key] = Flatten(pivotFacet.Value, string.Empty);
                }
            }

            return finalresults;
        }

        private ICollection<KeyValuePair<string, int>> Flatten(IEnumerable<Pivot> pivots, string parentName)
        {
            HashSet<KeyValuePair<string, int>> keyValuePairSet = new HashSet<KeyValuePair<string, int>>();
            foreach (Pivot pivot in pivots)
            {
                if (parentName != string.Empty)
                    keyValuePairSet.Add(new KeyValuePair<string, int>(parentName + "/" + pivot.Value, pivot.Count));
                if (pivot.HasChildPivots)
                    keyValuePairSet.UnionWith((IEnumerable<KeyValuePair<string, int>>)this.Flatten((IEnumerable<Pivot>)pivot.ChildPivots, pivot.Value));
            }
            return (ICollection<KeyValuePair<string, int>>)keyValuePairSet;
        }
    }
}
