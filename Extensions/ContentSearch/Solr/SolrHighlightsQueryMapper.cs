﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sandbox.Extensions.ContentSearch.Node;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Linq.Nodes;
using Sitecore.ContentSearch.Linq.Parsing;
using Sitecore.ContentSearch.Linq.Solr;
using SolrNet;
using SolrNet.Commands.Parameters;

namespace Sandbox.Extensions.ContentSearch.Solr
{
    public class SolrHighlightsQueryMapper : SolrQueryMapper
    {
        public SolrHighlightsQueryMapper(SolrIndexParameters parameters) : base(parameters)
        {
        }

        public override SolrCompositeQuery MapQuery(IndexQuery query)
        {
            if (query.RootNode.NodeType != QueryNodeType.Custom)
                return base.MapQuery(query);
            var highlightsNode = query.RootNode as WithSearchHighlightsNode;
            if (highlightsNode == null)
                return base.MapQuery(query);
            var compositeQuery = base.MapQuery(query);
            var queryWithHighlights = new SolrCompositeQueryWithHighlights(compositeQuery)
            {
                HighlightParameters = highlightsNode.HighlightFieldNames
                    .Select(p => this.Parameters.FieldNameTranslator.GetIndexFieldName(p)).ToArray()
            };

            return queryWithHighlights;
        }

        protected override AbstractSolrQuery Visit(QueryNode node, SolrQueryMapperState state)
        {
            // in this case we will do nothing, simply return the source node
            if (node.NodeType == QueryNodeType.Custom)
            {
                var highlightsNode = node as WithSearchHighlightsNode;
                if (highlightsNode != null)
                {
                    return Visit(highlightsNode.SourceNode, state);
                }
            }
            
            return base.Visit(node, state);
        }
    }


}