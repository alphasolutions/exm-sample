﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sandbox.Extensions.ContentSearch.Node;
using Sitecore.ContentSearch.Linq.Nodes;
using Sitecore.ContentSearch.Linq.Solr;

namespace Sandbox.Extensions.ContentSearch.Solr
{
    public class SolrHighlightQueryOptimizer : SolrQueryOptimizer
    {
        protected override QueryNode Visit(QueryNode node, SolrQueryOptimizerState state)
        {
            if (node.NodeType != QueryNodeType.Custom)
                return base.Visit(node, state);
            var highlightsNode = node as WithSearchHighlightsNode;
            return highlightsNode != null ? VisitWithinRadius(highlightsNode, state) : base.Visit(node, state);
        }

        private QueryNode VisitWithinRadius(WithSearchHighlightsNode highlightsNode, SolrQueryOptimizerState state)
        {
            return new WithSearchHighlightsNode(Visit(highlightsNode.SourceNode, state), highlightsNode.HighlightFieldNames);
        }
    }
}