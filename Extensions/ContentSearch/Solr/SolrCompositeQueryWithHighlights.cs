﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Linq.Solr;

namespace Sandbox.Extensions.ContentSearch.Solr
{
    public class SolrCompositeQueryWithHighlights : SolrCompositeQuery
    {
        public SolrCompositeQueryWithHighlights(SolrCompositeQuery query, GetResultsOptions options = GetResultsOptions.Default)
            : base(query.Query, query.Filter, query.Methods, query.VirtualFieldProcessors, query.FacetQueries, query.ExecutionContexts)
        {
            this.Methods.Insert(0, new GetResultsMethod(options));
        }

        public string[] HighlightParameters { set; get; }
    }
}