﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Sandbox.Extensions.ContentSearch.Node;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Extensions;
using Sitecore.ContentSearch.Linq.Nodes;
using Sitecore.ContentSearch.Linq.Parsing;

namespace Sandbox.Extensions.ContentSearch.Parsing
{
	public class ExpressionParserExtension : ExpressionParser
	{
	    public ExpressionParserExtension(Type elementType, Type itemType, FieldNameTranslator fieldNameTranslator) : base(elementType, itemType, fieldNameTranslator)
	    {
	    }


	    protected virtual QueryNode VisitGetResultsWithHighlightsMethod(MethodCallExpression methodCall)
	    {
	        QueryNode sourceNode = this.Visit(methodCall.Arguments[0]);
	        GetResultsOptions getResultsOptions = GetResultsOptions.Default;
	        if (methodCall.Arguments.Count >= 2)
	        {
	            QueryNode queryNode = this.Visit(this.GetArgument(methodCall.Arguments, 1));
	            if (queryNode.NodeType != QueryNodeType.Constant)
	                throw new NotSupportedException(
	                    $"Invalid get results options node type: {(object) queryNode.NodeType} - {(object) queryNode}");
	            getResultsOptions = (GetResultsOptions)((ConstantNode)queryNode).Value;
	        }
	        int num = (int)getResultsOptions;
	        return new GetResultsNode(sourceNode, (GetResultsOptions)num);
	    }
	    protected override QueryNode VisitMethodCall(MethodCallExpression methodCall)
	    {
	        var method = methodCall.Method;
	        if (method.DeclaringType == typeof(Linq.QueryableExtensions))
	        {
	            return this.VisitWithSearchHighlightsQueryableExtensionMethod(methodCall);
	        }
	        return base.VisitMethodCall(methodCall);
	    }

        protected virtual QueryNode VisitWithSearchHighlightsQueryableExtensionMethod(MethodCallExpression methodCall)
	    {
	        var sourceNode = Visit(GetArgument(methodCall.Arguments, 0));
	        var fieldNamesExpression = (ConstantExpression)GetArgument(methodCall.Arguments, 1);
            var fieldNames = (List<string>)fieldNamesExpression.Value;
	        if (methodCall.Arguments.Count >= 2)
	        {
	            QueryNode queryNode = this.Visit(this.GetArgument(methodCall.Arguments, 1));
	            if (queryNode.NodeType != QueryNodeType.Constant)
	                throw new NotSupportedException(
	                    $"Invalid get results options node type: {(object) queryNode.NodeType} - {(object) queryNode}");
	        }
	        return new WithSearchHighlightsNode(sourceNode, fieldNames);
	    }
    }
}