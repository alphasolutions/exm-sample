﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Sandbox.Extensions.ContentSearch.Linq;
using Sandbox.Extensions.ContentSearch.Node;
using Sandbox.Extensions.ContentSearch.SolrProviderWithHighlights;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Indexing;
using Sitecore.ContentSearch.Linq.Parsing;

namespace Sandbox.Extensions.ContentSearch.Parsing
{
	public class ExtendedGenericQueryable<TElement, TQuery> : GenericQueryable<TElement, TQuery>
    {
        public ExtendedGenericQueryable(Index<TElement, TQuery> index, QueryMapper<TQuery> queryMapper, IQueryOptimizer queryOptimizer, FieldNameTranslator fieldNameTranslator) :
            base(index, queryMapper, queryOptimizer, fieldNameTranslator)
        {
        }

        protected ExtendedGenericQueryable(Index<TQuery> index, QueryMapper<TQuery> queryMapper, IQueryOptimizer queryOptimizer, Expression expression, Type itemType, FieldNameTranslator fieldNameTranslator) :
            base(index, queryMapper, queryOptimizer, expression, itemType, fieldNameTranslator)
        {
        }

        public override IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            var genericQueryable = new ExtendedGenericQueryable<TElement, TQuery>(Index, QueryMapper, QueryOptimizer, expression, ItemType, FieldNameTranslator);
            ((IHasTraceWriter)genericQueryable).TraceWriter = ((IHasTraceWriter)this).TraceWriter;
            return genericQueryable;
        }

        protected override TQuery GetQuery(Expression expression)
        {
            Trace(expression, "Expression");
            var indexQuery = new ExpressionParserExtension(typeof(TElement), ItemType, FieldNameTranslator).Parse(expression);
            Trace(indexQuery, "Raw query:");
            var optimizedQuery = QueryOptimizer.Optimize(indexQuery);
            Trace(optimizedQuery, "Optimized query:");
            var nativeQuery = QueryMapper.MapQuery(optimizedQuery);
            Trace(new GenericDumpable(nativeQuery), "Native query:");
            return nativeQuery;
        }
    }
}