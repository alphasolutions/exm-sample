﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Modules.EmailCampaign.Recipients;

namespace Sandbox.Extensions.Recipients
{
    public class DynamicHtml : Property
    {
        public object Data { get; set; }
    }
}