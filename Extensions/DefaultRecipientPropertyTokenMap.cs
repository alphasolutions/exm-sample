﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign.Core.Personalization;
using Sitecore.Modules.EmailCampaign.Recipients;

namespace Sandbox.Extensions
{
    public class DefaultRecipientPropertyTokenMap : RecipientPropertyTokenMap
    {
        private static readonly Dictionary<Token, RecipientPropertyTokenBinding> TokenBindings = new[]
        {
            RecipientPropertyTokenBinding.Build(new Token("fullname"), (Expression<Func<PersonalInfo, object>>) (personalInfo => personalInfo.FullName)),
            RecipientPropertyTokenBinding.Build(new Token("name"), (Expression<Func<PersonalInfo, object>>) (personalInfo => personalInfo.FirstName)),
            RecipientPropertyTokenBinding.Build(new Token("firstname"), (Expression<Func<PersonalInfo, object>>) (personalInfo => personalInfo.FirstName)),
            RecipientPropertyTokenBinding.Build(new Token("lastname"), (Expression<Func<PersonalInfo, object>>) (personalInfo => personalInfo.LastName)),
            RecipientPropertyTokenBinding.Build(new Token("email"), (Expression<Func<Email, object>>) (email => email.EmailAddress)),
            RecipientPropertyTokenBinding.Build(new Token("phone"), (Expression<Func<Phone, object>>) (phone => phone.PhoneNumber)),
        //todo: update this
            //   RecipientPropertyTokenBinding.Build(new Token("CustomTokenExample"), )

        }.OrderBy(b => b.Token.Key).ToDictionary(b => b.Token, t => t);

        public override IList<RecipientPropertyTokenBinding> GetTokenBindings()
        {
            return TokenBindings.Values.ToList();
        }

        public override RecipientPropertyTokenBinding GetTokenBinding(Token token)
        {
            Assert.ArgumentNotNull(token, "token");
            RecipientPropertyTokenBinding propertyTokenBinding;
            TokenBindings.TryGetValue(token, out propertyTokenBinding);
            return propertyTokenBinding;
        }
    }
}