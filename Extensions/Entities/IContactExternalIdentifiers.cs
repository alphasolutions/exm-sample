﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Model.Framework;

namespace Sandbox.Extensions.Entities
{
   public interface IContactExternalIdentifiers : IFacet, IElement, IValidatable
    {
        IElementDictionary<IExternalSystemIdentifier> Entries { get; }

        string Preferred { get; set; }
    }
}
