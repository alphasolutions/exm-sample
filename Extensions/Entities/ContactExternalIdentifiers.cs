﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Model.Framework;

namespace Sandbox.Extensions.Entities
{
    [Serializable]
    public class ContactExternalIdentifiers : Facet, IContactExternalIdentifiers
    {

        public IElementDictionary<IExternalSystemIdentifier> Entries => GetDictionary<IExternalSystemIdentifier>("Entries");

        public string Preferred
        {
            get
            {
                return GetAttribute<string>("Preferred");
            }
            set
            {
                SetAttribute("Preferred", value);
            }
        }

        public ContactExternalIdentifiers()
        {
            EnsureAttribute<string>("Preferred");
            EnsureDictionary<IExternalSystemIdentifier>("Entries");
        }
    }
}