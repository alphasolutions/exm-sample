﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Model.Framework;

namespace Sandbox.Extensions.Entities
{
    internal class ExternalSystemIdentifier : Element, IExternalSystemIdentifier
    {
        public string Identifier
        {
            get
            {
                return GetAttribute<string>("ExternalIdentifier");
            }
            set
            {
                SetAttribute("ExternalIdentifier", value);
            }
        }
        public ExternalSystemIdentifier()
        {
            EnsureAttribute<string>("ExternalIdentifier");
        }
    }
}