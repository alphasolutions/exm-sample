﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Analytics.Model.Framework;

namespace Sandbox.Extensions.Entities
{
    public interface IExternalSystemIdentifier : IElement, IValidatable
    {
        string Identifier { get; set; }

    }
}
