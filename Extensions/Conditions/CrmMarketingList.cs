﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Sitecore;
using Sitecore.Analytics.Rules.SegmentBuilder;
using Sitecore.ContentSearch.Analytics.Models;
using Sitecore.ContentSearch.Rules.Conditions;

namespace Sandbox.Extensions.Conditions
{
    public class CrmMarketingList<T> : TypedQueryableOperatorCondition<T, IndexedContact> where T : VisitorRuleContext<IndexedContact>
    {
        public string Value { get; set; }

        protected override Expression<Func<IndexedContact, bool>> GetResultPredicate(T ruleContext)
        {
            if (this.Value == null)
                return c => false;
            return this.GetCompareExpression(c => c["crm.marketinglist"], Value);
        }
    }
}