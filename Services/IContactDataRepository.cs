﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Extensions.Entities;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Tracking;
using Sitecore.Modules.EmailCampaign.Xdb;

namespace Sandbox.Services
{
    public interface IContactDataRepository
    {
        string GetEmailForContact(Contact contact);
        string GetExternalSystemIdentifier(Contact contact);
        XdbContactId GetXdbContactId(string email);
        void UpdateEmail(Contact contact, string email);
    }
}
