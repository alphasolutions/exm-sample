﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sandbox.Extensions.Entities;
using Sitecore.Analytics;
using Sitecore.Analytics.Data;
using Sitecore.Analytics.DataAccess;
using Sitecore.Analytics.Model;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Tracking;
using Sitecore.Data;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Modules.EmailCampaign.Core.Gateways;
using Sitecore.Modules.EmailCampaign.Factories;
using Sitecore.Modules.EmailCampaign.Xdb;

namespace Sandbox.Services
{
    public class ContactDataRepository : IContactDataRepository
    {
        private readonly AnalyticsGateway _gateway;
        private readonly ContactRepositoryBase _contactRepository;
        public ContactDataRepository()
        {
            _gateway = EcmFactory.GetDefaultFactory().Gateways.AnalyticsGateway;
            _contactRepository = (ContactRepositoryBase)Sitecore.Configuration.Factory.CreateObject("contactRepository", true);

        }

        public string GetEmailForContact(Contact contact)
        {
            var emailFacet = contact.GetFacet<IContactEmailAddresses>(Constants.ContactFacetNames.Emails);
            return !string.IsNullOrEmpty(emailFacet.Preferred) && emailFacet.Entries.Keys.Any() && emailFacet.Entries.Contains(emailFacet.Preferred) ? emailFacet.Entries[emailFacet.Preferred].SmtpAddress :
                emailFacet.Entries[emailFacet.Entries.Keys.FirstOrDefault()].SmtpAddress;
        }

        public string GetExternalSystemIdentifier(Contact contact)
        {
            var externalSystemIdentifierFacet = contact.GetFacet<IContactExternalIdentifiers>(Constants.ContactFacetNames.ContactExternalSystemIdentifiers);
            return externalSystemIdentifierFacet == null ||
                   !externalSystemIdentifierFacet.Entries.Contains("External System 1")
                ? "No Data Provided"
                : externalSystemIdentifierFacet.Entries["External System 1"].Identifier;
        }

        public XdbContactId GetXdbContactId(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                if (Sitecore.Context.IsLoggedIn && Sitecore.Context.User.Profile.Email == email)
                    return new XdbContactId(Tracker.Current.Contact.ContactId);
                var anonymousIdFromEmail = ClientApi.GetAnonymousIdFromEmail(email);
                if (anonymousIdFromEmail.HasValue)
                {
                    return new XdbContactId(new ID(anonymousIdFromEmail.Value));
                }
                // if the contact does not exist in Xdb then create one
                var newContactId = _gateway.CreateContact(email);
                if (newContactId != Guid.Empty)
                {
                    LeaseOwner leaseOwner = new LeaseOwner("GetXdbContactId-" + Guid.NewGuid(),
                        LeaseOwnerType.OutOfRequestWorker);
                    TimeSpan leaseDuration = TimeSpan.FromSeconds(15.0);
                    TimeSpan timeout = leaseDuration;
                    Contact contact;
                    string webClusterName;
                    switch (
                        _gateway.TryGetContactForUpdate(ID.Parse(newContactId), leaseOwner, leaseDuration, timeout,
                            out contact, out webClusterName))
                    {
                        case ContactLockingStatus.InCurrentTracker:

                            UpdateEmail(contact, email);
                            break;

                        case ContactLockingStatus.LockAcquired:
                            try
                            {
                                UpdateEmail(contact, email);

                            }
                            finally
                            {
                                _contactRepository.SaveContact(contact,
                                    new ContactSaveOptions(true, leaseOwner, new TimeSpan?(leaseDuration)));
                            }
                            break;
                        case ContactLockingStatus.LockedByWebCluster:
                        // we will probably never run into this as the contact is new

                        case ContactLockingStatus.NotFound:
                        //handle error
                        default:
                            //handle error
                            break;
                    }
                    return new XdbContactId(newContactId);
                }
            }
            return null;

            #region unused code

            #endregion
        }

        public void UpdateEmail(Contact contact, string email)
        {
            var emailFacet = contact.GetFacet<IContactEmailAddresses>(Constants.ContactFacetNames.Emails);

            var emailaddress = !emailFacet.Entries.Contains("Work Email")
                ? emailFacet.Entries.Create("Work Email")
                : emailFacet.Entries["Work Email"];
            emailaddress.SmtpAddress = email;
            emailFacet.Preferred = "Work Email";

            //sample code for updating  user info
            var contactInfo = contact.GetFacet<IContactPersonalInfo>(Constants.ContactFacetNames.Personal);
            contactInfo.FirstName = "Created";
            contactInfo.Surname = "ByCode";

            //tempCode for saving external identifier

            var externalSystemIdentifierFacet = contact.GetFacet<IContactExternalIdentifiers>(Constants.ContactFacetNames.ContactExternalSystemIdentifiers);
            if (externalSystemIdentifierFacet != null)
            {
                var identifierFacet = externalSystemIdentifierFacet.Entries.Contains("External System 1")
                    ? externalSystemIdentifierFacet.Entries["External System 1"]
                    : externalSystemIdentifierFacet.Entries.Create("External System 1");
                identifierFacet.Identifier = "Add the identifier here";
            }
        }
    }
}