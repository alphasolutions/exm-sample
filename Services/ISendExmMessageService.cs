﻿using System.Collections.Generic;
using Sitecore.Data;
using Sitecore.Modules.EmailCampaign.Xdb;

namespace Sandbox.Services
{
    public interface ISendExmMessageService
    {

        void Send(ID messageItemId, string email, Dictionary<string, object> customPersonsTokens);

      
    }
}
