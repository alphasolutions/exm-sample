﻿using System;
using System.Collections.Generic;
using Sandbox.Extensions.Entities;
using Sitecore.Analytics;
using Sitecore.Analytics.Data;
using Sitecore.Analytics.DataAccess;
using Sitecore.Analytics.Model;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Tracking;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Modules.EmailCampaign.Core.Gateways;
using Sitecore.Modules.EmailCampaign.Factories;
using Sitecore.Modules.EmailCampaign.Recipients;
using Sitecore.Modules.EmailCampaign.Xdb;

namespace Sandbox.Services
{
    public class SendExmMessageService : ISendExmMessageService
    {
        private readonly IContactDataRepository _contactDataRepository;
        public SendExmMessageService(IContactDataRepository contactDataRepository)
        {
            _contactDataRepository = contactDataRepository;
        }
        public void Send(ID messageItemId, string email, Dictionary<string, object> customPersonsTokens)
        {
            // retrive message item from Sitecore
            var message = Factory.GetMessage(messageItemId);
            // for multi lingual solution use the override to get the correct language version
            //var message = Factory.GetMessage(messageItemId,"en");
            Assert.IsNotNull(message, $"Could not find message with ID {messageItemId}");
            RecipientId recipient = _contactDataRepository.GetXdbContactId(email);
            Assert.IsNotNull(recipient, $"Could not find recipient with email {email} in xDB");

            foreach (var customPersonToken in customPersonsTokens as IEnumerable<KeyValuePair<string, object>>)
            {
                message.CustomPersonTokens[customPersonToken.Key] = customPersonToken.Value;
            }
            // sync call
            //var sendingManager = new SendingManager(message);
            //sendingManager.SendStandardMessage(recipient);
            var asyncSending = new AsyncSendingManager(message);
            asyncSending.SendStandardMessage(recipient);
        }

    }
}